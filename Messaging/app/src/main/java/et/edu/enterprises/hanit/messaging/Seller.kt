package et.edu.enterprises.hanit.messaging

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Switch
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_seller.*

class Seller : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_seller)
        var flagf = intent.getIntExtra("flag",0)
        println(flagf)
        var tvs1=findViewById<View>(R.id.tvs1) as Switch
        var tvs2=findViewById<View>(R.id.tvs2) as Switch
        var tvs3=findViewById<View>(R.id.tvs3) as Switch
        if (flagf==1){
            tvn1.setText("V8")
            tvp1.setText("$16,245")
            tvs1.setText("Sold")
            tvs1.setOnClickListener{
            if(tvs1.isChecked){
                Toast.makeText(this@Seller,"V8 SOLD",Toast.LENGTH_SHORT).show()
            }
        }}
        else if (flagf==2){
            tvn1.setText("V8")
            tvp1.setText("$16,245")
            tvs1.setText("Sold")
            tvn2.setText("Toyota Corolla")
            tvp2.setText("$18,745")
            tvs2.setText("Sold")
            tvn3.setText("Volkswagen Beetle")
            tvp3.setText("$12,405")
            tvs3.setText("Sold")
            tvs1.setOnClickListener{
                if(tvs1.isChecked){
                    Toast.makeText(this@Seller,"V8 SOLD",Toast.LENGTH_SHORT).show()
                }
            }
            tvs2.setOnClickListener{
                if(tvs2.isChecked){
                    Toast.makeText(this@Seller,"Toyota Corolla SOLD",Toast.LENGTH_SHORT).show()
                }
            }
            tvs3.setOnClickListener{
                if(tvs3.isChecked){
                    Toast.makeText(this@Seller,"Volkswagen Beetle SOLD",Toast.LENGTH_SHORT).show()
                }
            }
        }
        if (flagf==3){
            tvn1.setText("None")
            tvp1.setText("None")
            tvn2.setText(" ")
            tvp2.setText(" ")
            tvn3.setText(" ")
            tvp3.setText(" ")
        }
        if (flagf==4){
            tvn1.setText("V8")
            tvp1.setText("$16,245")
            tvs1.setText("Sold")
            tvn2.setText("Volkswagen Beetle")
            tvp2.setText("$12,405")
            tvs2.setText("Sold")
            tvs1.setOnClickListener{
                if(tvs1.isChecked){
                    Toast.makeText(this@Seller,"V8 SOLD",Toast.LENGTH_SHORT).show()
                }
            }
            tvs3.setOnClickListener{
                if(tvs3.isChecked){
                    Toast.makeText(this@Seller,"Volkswagen Beetle SOLD",Toast.LENGTH_SHORT).show()
                }
            }
        }
        if (flagf==5){
            tvn1.setText("Toyota Corolla")
            tvp1.setText("$18,745")
            tvs1.setText("Sold")
            tvs2.setOnClickListener{
                if(tvs2.isChecked){
                    Toast.makeText(this@Seller,"Toyota Corolla SOLD",Toast.LENGTH_SHORT).show()
                }
            }
        }
        if (flagf==6){
            tvn1.setText("Toyota Corolla")
            tvp1.setText("$18,745")
            tvs1.setText("Sold")
            tvn2.setText("Volkswagen Beetle")
            tvp2.setText("$12,405")
            tvs2.setText("Sold")
            tvs2.setOnClickListener{
                if(tvs2.isChecked){
                    Toast.makeText(this@Seller,"Toyota Corolla SOLD",Toast.LENGTH_SHORT).show()
                }
            }
            tvs3.setOnClickListener{
                if(tvs3.isChecked){
                    Toast.makeText(this@Seller,"Volkswagen Beetle SOLD",Toast.LENGTH_SHORT).show()
                }
            }
        }
        if (flagf==7){
            tvn1.setText("V8")
            tvp1.setText("$16,245")
            tvs1.setText("Sold")
            tvn2.setText("Toyota Corolla")
            tvp2.setText("$18,745")
            tvs2.setText("Sold")
            tvs1.setOnClickListener{
                if(tvs1.isChecked){
                    Toast.makeText(this@Seller,"V8 SOLD",Toast.LENGTH_SHORT).show()
                }
            }
            tvs2.setOnClickListener{
                if(tvs2.isChecked){
                    Toast.makeText(this@Seller,"Toyota Corolla SOLD",Toast.LENGTH_SHORT).show()
                }
            }
        }
        if (flagf==8){

            tvn1.setText("Volkswagen Beetle")
            tvp1.setText("$12,405")
            tvs1.setText("Sold")
            tvs3.setOnClickListener{
                if(tvs3.isChecked){
                    Toast.makeText(this@Seller,"Volkswagen Beetle SOLD",Toast.LENGTH_SHORT).show()
                }
            }
        }
        }
}
