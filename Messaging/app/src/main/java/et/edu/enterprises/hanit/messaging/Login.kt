package et.edu.enterprises.hanit.messaging

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_login2.*

class Login : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login2)
        buyer_button.setOnClickListener{
            val intent= Intent(this,MainActivity::class.java)
            startActivity(intent)
        }
        Seller_button.setOnClickListener{
            var myflag=intent.getIntExtra("flag",0)
            val intent= Intent(this,Seller::class.java)
            intent.putExtra("flag",myflag)
            startActivity(intent)
        }

    }
}
