package et.edu.enterprises.hanit.messaging

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.CheckBox
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_seller.*

class MainActivity : AppCompatActivity() {
   lateinit var v8 : CheckBox
    lateinit var TC : CheckBox
    lateinit var VB : CheckBox
    lateinit var buy_btn: Button
    var flag:Int =0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main
        )
        buy_button.setOnClickListener{
            if ((v8.isChecked())&&!(TC.isChecked())&&!(VB.isChecked())) {
                            flag=1
                        }
            else if ((v8.isChecked())&&(TC.isChecked())&&(VB.isChecked())) {
                flag=2
            }
            else if (!(v8.isChecked())&&!(TC.isChecked())&&!(VB.isChecked())) {
                flag=3
            }
            else if ((v8.isChecked())&&!(TC.isChecked())&&(VB.isChecked())) {
                flag=4
            }
            else if (!(v8.isChecked())&&(TC.isChecked())&&!(VB.isChecked())) {
                flag=5
            }
            else if (!(v8.isChecked())&&(TC.isChecked())&&(VB.isChecked())) {
                flag=6
            }
            else if ((v8.isChecked())&&(TC.isChecked())&&!(VB.isChecked())) {
                flag=7
            }
            else if (!(v8.isChecked())&&!(TC.isChecked())&&(VB.isChecked())) {
                flag = 8
            }
            val intentt= Intent(this,Login::class.java)
             intentt.putExtra("flag",flag)
            startActivity(intentt)

        }
               v8= findViewById(R.id.v8) as CheckBox
                TC=findViewById(R.id.Toyota_Corolla) as CheckBox
                VB=findViewById(R.id.Volkswagen_Beetle) as CheckBox
                buy_btn=findViewById(R.id.buy_button) as Button

        }

}


